import sys


class Calculadora():
    def __init__(self, valor1, operacion, valor2):

        self.valor1 = valor1
        self.operacion = operacion
        self.valor2 = valor2

    def suma(self):
        return int(self.valor1) + int(self.valor2)

    def resta(self):
        return int(self.valor1) - int(self.valor2)


class CalculadoraHija(Calculadora):

    def multi(self):
        return int(self.valor1) * int(self.valor2)

    def divi(self):
        if int(self.valor2) == 0:
            return("Division by 0 is not allowed")
        else:
            return int(self.valor1) / int(self.valor2)

    def operaciones(self):
        if self.operacion == 'suma':
            return self.suma()

        elif self.operacion == 'resta':
            return self.resta()

        elif self.operacion == 'multiplica':
            return self.multi()

        elif self.operacion == 'divide':
            return int(self.divi())

        else:
            return('no valido')

if __name__ == "__main__":
    try:

        valor1 = sys.argv[1]
        operacion = sys.argv[2]
        valor2 = sys.argv[3]

    except ValueError:
        sys.exit("Error del carajo")

    objeto = CalculadoraHija(int(valor1), operacion, int(valor2))
    print(objeto.operaciones())

