import sys


class Calculadora():

    def __init__(self, valor1, operacion, valor2):
        self.valor1 = valor1
        self.operacion = operacion
        self.valor2 = valor2

    def suma(self):

        return int(self.valor1) + int(self.valor2)

    def resta(self):

        return int(self.valor1) - int(self.valor2)

    def operaciones(self):

        if self.operacion == 'suma':
            return (objeto.suma())

        elif self.operacion == 'resta':
            return (objeto.resta())

        else:
            return("No es valido")

if __name__ == "__main__":

    try:
        operacion = sys.argv[2]
        valor1 = sys.argv[1]
        valor2 = sys.argv[3]

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = Calculadora(int(valor1), operacion, int(valor2))
    print(objeto.operaciones())
