import calcoohija
import csv

if __name__ == "__main__":

    with open('fichero.csv') as csvfile:
        fichero = csv.reader(csvfile)

        for line in fichero:
            print(line[0])
            operacion = line[0]
            result = line[1]

            for valores in line[2:]:
                calc = calcoohija.CalculadoraHija(result, operacion, valores)
                result = calc.operaciones()

            print(result)
